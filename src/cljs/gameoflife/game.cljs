(ns gameoflife.game)

(def alive? (complement zero?))

(defn visible-grid-coordinates [max c]
  (cond
    (< c 0)   max
    (> c max) 0
    :else     c))

(defn get-cell [board coords]
  (let [size (count board)]
    (get-in board (map (partial visible-grid-coordinates (dec size))
                       coords))))

(def neighbor-deltas
  [[-1 -1] [-1 0] [-1 1]
   [0 -1] [0 1] 
   [1 -1] [1 0] [1 1]])

(defn neighbor-coordinates [y x]
  (map (fn [[dx dy]] [(+ y dy) (+ x dx)]) neighbor-deltas))

(defn count-neighbors [board y x]
  (->> (neighbor-coordinates y x)
       (map (partial get-cell board))
       (filter alive?)
       (count)))

(defn got-to-live? [board y x cell]
  (let [neighbors (count-neighbors board y x)]
    (if (alive? cell)
      (<= 2 neighbors 3)
      (== 3 neighbors))))

(defn map-grid [f grid]
  (vec (map-indexed (fn [y row]
                      (vec (map-indexed (fn [x cell]
                                          (f grid y x cell)) row))) grid)))

(defn next-generation-tick [board]
  (map-grid (comp js/Number got-to-live?) board))

