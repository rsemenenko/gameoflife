(ns gameoflife.events
  (:require
   [re-frame.core :as re-frame]
   [gameoflife.db :as db]
   [gameoflife.game :as game]
   ))

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::set-grid-size
 (fn [db [_ size]]
   (assoc db :grid-size size)))

(re-frame/reg-event-db
 ::tick
 (re-frame/path [:board])
 game/next-generation-tick)

(re-frame/reg-event-db
 ::start
 (fn [db _]
   (assoc db :started true)))

(re-frame/reg-event-db
 ::place
 (fn [db [_ y x v]]
   (update-in db [:board y x] (constantly v))))
