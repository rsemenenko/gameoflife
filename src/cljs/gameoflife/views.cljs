(ns gameoflife.views
  (:require
   [re-frame.core :as re-frame]
   [gameoflife.subs :as subs]
   [gameoflife.events :as events]
   [gameoflife.game :as game]))

(defn place-living [started y x]
  (when-not started (re-frame/dispatch [::events/place y x 1])))

(defn cell-view [board y x cell]
  (let [started (re-frame/subscribe [::subs/started])]
    [:td.cell {:key (str x y)
               :on-click #(place-living @started y x)}
     (if (game/alive? cell) "*" ".")]))

(defn row-view [board y row]
  [:tr {:key y} (map-indexed (partial cell-view board y) row)])

(defn grid [board]
  [:tbody (map-indexed (partial row-view board) board)])

(defn live-long-and-prosper [started]
  (when-not started (re-frame/dispatch [::events/start]))
  (re-frame/dispatch [::events/tick]))

(defn main-panel []
  (let [board   (re-frame/subscribe [::subs/board])
        started (re-frame/subscribe [::subs/started])]
    [:div.root
     [:div.game
      [:h1 "Game of Life"]
      [:table (grid @board)]]
     [:div
      [:h1 "Controls"]
      [:button {;;:disabled @started
                :on-click (partial live-long-and-prosper @started)} "Next"]]]))
