(ns gameoflife.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::board
 (fn [db]
   (:board db)))

(re-frame/reg-sub
 ::grid-size
 (fn [db]
   (:grid-size db)))

(re-frame/reg-sub
 ::started
 (fn [db]
   (:started db)))
